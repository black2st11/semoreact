import { createEpicMiddleware, combineEpics } from 'redux-observable';

import { combineReducers, createStore, applyMiddleware} from 'redux';

import { authInfoReducer, userAuthEpic, getProfileEpic  } from './SubComponents/auth'
const rootReducers = () => combineReducers({
    authInfoReducer,
})

const rootEpics =  combineEpics(
    userAuthEpic,
    getProfileEpic,
)

const epicMiddleware = createEpicMiddleware();

export default function configureStore() {
    const store = createStore(
        rootReducers(),
        applyMiddleware(epicMiddleware)
    );

    epicMiddleware.run(rootEpics);

    return store;
};