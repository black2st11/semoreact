import React, {useState} from 'react'
import {Input, Button, Layout, Typography, Alert, Modal} from 'antd'
import {Link} from 'react-router-dom'
// import userAuthEpic from '../redux/auth'
import axios from 'axios'
import {useHistory} from 'react-router-dom'
// export default class Login extends React.Component{

//     render(){
//         const [id, setId] = useState('');
//         console.log(id)
//         return(
//             <div>
//                 <Input placeholder="ID" onChange={(e)=>setId(e)} />
//                 <Input.Password placeholder="PASSWORD" />
//                 <Button>회원가입</Button>
//                 <Button>로그인</Button>
//             </div>
//         )
//     }
// }
const {Header, Content, Footer} = Layout
const {Title} = Typography
export default function Login(){
    let history = useHistory()
    const [id, setId] = useState('')
    const [password, setPassword] = useState('')
    const client_id = 'WsckrtEQtdj9IzWm2sCZhwfdROv9ty7Dnk4qJIeP'
    const secret_id = '7uulbYH6LbTsbQhiAC4GrzTEIvDZs1VmDAM5qCJDFjV1zrZdqt6unUuUWwxjaQ3mH8oPNtbvSrwCEAhSv3aBZKRnyA0G4yvmUaPidSKueAWQ2d96M6oxg4GjRDO180vD'
    const _data = "grant_type=password&username="+id+"&password="+password+"&client_id="+client_id+"&secret_id="+secret_id
    const payload = {
        "grant_type": "password",
        "username" : id,
        "password" : password,
        "client_id" : client_id,
        "secret_id" : secret_id,
    }
    const handleSubmit = async() =>{
        // await axios.post('http://localhost:8000/o/token', {data:1})
        await axios({
            url: 'http://localhost:8000/o/custom_token/',
            method: "POST",
            data: _data,
            headers:{
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            withCredentials: true
        }).then((response)=>{
            localStorage.setItem('access_token',response.data.access_token)
            localStorage.setItem('username',id)
            history.push('/home')
        }).catch(function(err){
            if(err.response.data.error=='invalid_grant'){
                return Modal.warning({
                    title:'로그인에 실패하였습니다.',
                    content :'아이디와 비밀번호를 확인해주세요'
                })
            }else{
                return Modal.warning({
                    title:'서버오류로 현재 서비스 이용이 불가합니다.',
                    content :'잠시후 다시 이용해주세요'
                })
            }
        })
    }
    
    return(
        <Layout className='container'>
            <Content style={{width:'100%', textAlign:"center", margin:'auto'}} >
                <div style={{margin:'25%'}}>
                    <div style={{textAlign:'center', width:'100%'}}>
                    <Title style={{width:'100%', fontSize: '5rem'}}>세모가펑</Title>
                    </div>
                    <div className='login-form' style={{width:'100%'}}>
                        <Input placeholder="ID" value={id} onChange={(e)=>setId(e.target.value)}  />
                        <Input.Password placeholder="PASSWORD" style={{marginTop:20}} value={password} onChange={(e)=>setPassword(e.target.value)} />
                        <div style={{marginTop:20}}>
                            <Link to='/signup'>
                                <Button>회원가입</Button>
                            </Link>
                            <Button onClick={handleSubmit} style={{marginLeft:10}}>로그인</Button>
                        </div>
                    </div>
                </div>
            </Content>
        </Layout>
    )
}