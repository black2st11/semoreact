import React,{useEffect} from 'react'
import axios from 'axios'
export default function Board(){
    const getBoardList= ()=>{
        axios({
            method:"GET",
            headers : {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            },
            url : 'http://localhost:8000/board/'
        }).then(res=>console.log(res))
    }
    useEffect(()=>{
        getBoardList()
    },[])
    return (
        <div>Board
        <ul>
            <li>title-owner</li>
        </ul>
        </div>
    )
}