import React, {useEffect} from 'react'
import axios from 'axios'
export default function BoardDetail(){
    const board_id = 0
    const getBoardDetailInfo=()=>{
        axios({
            method:'GET',
            url : `http://localhost:8000/board/${board_id}/`,
            headers : {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            },
        }).then(res=>console.log(res))
    }
    useEffect(()=>{
        getBoardDetailInfo()
    }, [])
    return (
        <div>
            <p>보드 디테일 </p>
            <p>title</p>
            <p>content</p>
            <p>create_time</p>
            <p>owner</p>
        </div>
    )
}