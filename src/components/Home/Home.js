import React,{useState,useEffect} from 'react'
import axios from 'axios'
import {Layout, PageHeader, Typography, Button } from 'antd'
import {Link, useHistory} from 'react-router-dom'

const {Title} = Typography
export default function Home(){
    const history = useHistory()

    const handel = ()=>{
        axios({
            headers : {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            },
            url : 'http://localhost:8000/home/'
        }).then(Response=>console.log(Response))
        .catch(function(err){
            if(err.response.status==401){
                history.push('/')
            }
        })
    }
    useEffect(()=>{
        handel()
    })
    const logout = () =>{
        console.log('rm access_token')
        localStorage.removeItem('access_token')
        history.push('/')
    }
    return (
    <div>
        <PageHeader title='홈' extra={[<Button onClick={logout} key={1}>로그아웃</Button>]} />
        <Layout className='container'>
            <div className='white' style={{width:'50%',height:100,display:'flex' ,justifyContent:'center',alignItems:'center', borderRadius:5, border:'1px solid #cecaca' ,backgroundColor:'white'}}>
            <Link to='/valuation'>
                <Title style={{paddingTop:10, color:'gray'}}>평가하기</Title>
            </Link> 
            </div>
            <div className='white' style={{width:'50%',height:100,display:'flex' ,justifyContent:'center',alignItems:'center', borderRadius:5, border:'1px solid #cecaca',marginTop:10 ,backgroundColor:'white'}}>
                <Link to='/upload' >
                    <Title style={{paddingTop:10, color:'gray'}}>평가등록</Title>
                </Link>
            </div>
            <div className='white' style={{width:'50%',height:100,display:'flex' ,justifyContent:'center',alignItems:'center', borderRadius:5, border:'1px solid #cecaca',marginTop:10 ,backgroundColor:'white'}}>
                <Link to='/rank'>
                <Title style={{paddingTop:10, color:'gray'}}>랭킹보기</Title>
                </Link>
            </div>
            <div className='white' style={{width:'50%',height:100,marginTop:10, display:'flex' ,justifyContent:'center',alignItems:'center', borderRadius:5, border:'1px solid #cecaca' ,backgroundColor:'white'}}>
                <Link to='/mypage' >
                <Title style={{paddingTop:10, color:'gray'}}>마이세팅</Title>
                </Link>
            </div>
            
        </Layout>
    </div>
    
    )
}