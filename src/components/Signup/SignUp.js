import React,{useState} from 'react'
import {Input, Button, Layout, PageHeader,notification} from 'antd'
import {useHistory} from 'react-router-dom'

import axios from 'axios'

const {Content} = Layout
export default function SignUp(){

    // const [email, setEmail] = useState('')
    const useInput = initialValue => {
        const [value, setValue] = useState(initialValue)
        const onChange = event => {
            const {target : {value}} = event
            setValue(value)

        }
        return {value, onChange}
    }
    const id = useInput('')
    const email = useInput('')
    const password = useInput('')
    const confirmPassword = useInput('')
    const history = useHistory()

    const handleSubmit = () =>{
        console.log(password)
        console.log(confirmPassword)
        if (password.value==confirmPassword.value){
            axios({
                url: 'http://localhost:8000/user/',
                method : "POST",
                data : {
                    'username' : id.value,
                    'password' : password.value,
                    'email' : email.value
                },
                Headers:{
                    'Content-type' : 'application/json'
                },
                withCredentials : true,
                timeout : 10000
            }).then(response=>history.push('/'))
            .catch(function(err){
                console.log(err)
            })
        }else{
            notification.warn({message:'비밀번호가 일치하지 않습니다.'})
        }
        
    }
    return(
        <div>
            <PageHeader onBack={()=>history.push('/')} title='로그인화면으로 이동' />
            <Layout className="container">
                <Content style={{width:'100%'}}>
                    <div style={{margin:'25%', textAlign:'center'}}>
                        <Input addonBefore="EMAIL" style={{marginTop:10}} placeholder='이메일을 입력해주세요' {...email}/>            
                        <Input addonBefore="ID" style={{marginTop:10}} placeholder='ID를 입력해주세요' {...id}/>
                        <Input.Password addonBefore="PASSWORD" style={{marginTop:10}} placeholder="비밀번호를 입력해주세요" {...password}/>
                        <Input.Password addonBefore="RE-PASSWORD" style={{marginTop:10}} placeholder='비밀번호를 다시 한번 입력해주세요'{...confirmPassword}/>
                        <Button style={{marginTop:10}} onClick={handleSubmit}>회원가입</Button>
                    </div>
                    
                </Content>
            </Layout>
        </div>
        
    )
}