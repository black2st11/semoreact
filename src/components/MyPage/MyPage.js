import React,{useEffect, useState} from 'react'
import axios from 'axios'
import {Button,Modal,Card, Icon, Avatar,PageHeader,Layout, Input, Table, notification} from 'antd'
import {useHistory} from 'react-router-dom'
const {Content} = Layout
export default function MyPage(){
    useEffect(()=>{
        getInfo()
        getValuation()
    },[])
    const openNotificationWithIcon = type => {
        if (type=='success'){
            notification[type]({
                message: '비밀번호 변경이 완료되었습니다.',
              });
        }else if(type=='warning'){
            notification[type]({
                message: '비밀번호가 일치하지않습니다.',
              });
        }else{
            notification[type]({
                message: '비밀번호 변경에 실패했습니다.',
              });
        }
        
      };

    const getInfo = () =>{
        axios({
            method:'GET',
            url : 'http://localhost:8000/user/',
            headers : {
                'Content-type' : 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        }).then(res=>{
            setId(res.data.username)
            setEmail(res.data.email)
        })
        .catch(function(err){
            console.log(err)
        })
    }
    const getValuation = () =>{
        axios({
            method:'GET',
            url :'http://localhost:8000/myvaluations/',
            headers:{
                'Content-type' : 'application/json',
                'Authorization' : `Bearer ${localStorage.getItem('access_token')}`
            }
        }).then(res=>setResult(res.data))
        .catch(err=>console.log(err))
    }
    const delValuation = (id) =>{
        axios({
            method:'DELETE',
            url :`http://localhost:8000/valuations/${id}/`,
            headers:{
                'Content-type' : 'application/json',
                'Authorization' : `Bearer ${localStorage.getItem('access_token')}`
            }
        }).then(res=> getValuation())
        .catch(err=>console.log(err))
    }
    const setIsVisible = () =>{
        console.log(visible)
        if(visible){
            setVisible(false)
            setRePassword(null)
            setPassword(null)
        }else{
            setVisible(true)
        }
    }
    const putPassword= () =>{
        if(repassword==password){
            axios({
                method : 'PUT',
                url : 'http://localhost:8000/pass/',
                data : {'password':password},
                headers : {
                    'Content-type' : 'application/json',
                    'Authorization' : `Bearer ${localStorage.getItem('access_token')}`
                }
            }).then(res=>{
                setIsVisible()
                openNotificationWithIcon('success')

            }).catch(function(err){
                openNotificationWithIcon('error')
                console.log(err)
            })
        }else{
            openNotificationWithIcon('warning')
        }
    }
    const columns_1 = [
        {
            title : '평가이름',
            dataIndex : 'name',
            key :'name',
        },
        {
            title : '원래가격',
            dataIndex : 'price',
            key : 'price',
            render: (text) => <p>{parseInt(text).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}원</p>
        },
        {
            title : '이미지',
            dataIndex :'image',
            key : 'image',
            render : (text)=><img style={{width:50, height:50}} src={'http://localhost:8000/media/'+text} />
        },
        {
            title : '예상평균가격',
            dataIndex : 'average_price',
            key : 'average_price',
            render : (text) => <p>{parseInt(text).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}원</p>
        }
    ]
    const columns_2 = [
        {
            title : '평가이름',
            dataIndex : 'name',
            key :'name',
        },
        {
            title : '원래가격',
            dataIndex : 'price',
            key : 'price',
            render: (text) => <p>{parseInt(text).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}원</p>
        },
        {
            title : '이미지',
            dataIndex :'image',
            key : 'image',
            render : (text)=><img style={{width:50, height:50}} src={'http://localhost:8000/:8000/media/'+text} />
        },
        {
            title:'삭제',
            dataIndex : 'id',
            key : 'id',
            render : (text) => <Button onClick={()=>delValuation(text)}>삭제</Button>
        }
    ]
    const history = useHistory()
    const [email, setEmail] =  useState()
    const [result, setResult] =  useState(null)
    const [id, setId] =  useState()
    const [password, setPassword] =  useState()
    const [repassword, setRePassword] =  useState()
    const [visible, setVisible] =  useState(false)
    
    return(
        <div>
            <PageHeader onBack={()=>history.push('/home')} title='마이페이지' />
            <Layout className='container' style={{height:'100%'}} >
                <Content style={{width:'50%', marginLeft:'25%', marginRight:'25%'}}>
                    <Card 
                        style={{width:300, marginTop:10,marginLeft:'25%'}}
                        cover={<img alt='img' src='https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png' />}
                        actions ={[<Button style={{border:0}} onClick={setIsVisible} ><Icon type='setting' key='setting'/></Button>]}
                        >
                        <p>ID : {id}</p>
                        <p>EMAIL : {email}</p>
                    </Card>
                    <Modal 
                        visible={visible}
                        onCancel={setIsVisible}
                        onOk={putPassword}
                        okText='비밀번호 변경'
                        title='비밀번호 변경'
                    >
                        <Input.Password value={password} onChange={e=>setPassword(e.target.value)} style={{marginTop:10}} placeholder='비밀번호를 입력해주세요'  />
                        <Input.Password value={repassword} onChange={e=>setRePassword(e.target.value)} style={{marginTop:10}} placeholder='다시 비밀번호를 입력해주세요' />
                    </Modal>
                    <p>평가받은 항목</p>
                    {result==null?null:<Table style={{width:'100%', marginTop:10}} columns={columns_1} dataSource={result.data} />}
                    <p>등록한 평가</p>
                    {result==null?null:<Table style={{width:'100%', marginTop:10}} columns={columns_2} dataSource={result.origin_data} />}

                </Content>
            </Layout>
        </div>

    )
}