import React from 'react'
import {Upload, message, Icon, Button, Input, PageHeader, Layout,Modal, Tag} from 'antd'
import { PlusOutlined } from '@ant-design/icons';

import axios from 'axios'
import {useHistory} from 'react-router-dom'
const {Conetent} = Layout

  




export default class UpValuation extends React.Component{
    constructor(props){
        super(props);
        this.state={
            loading : false,
            image : null,
            name : '',
            price : null,
            tags : [],
            inputVisible :false,
            inputValue : ''
        }
    }

    handleClose = removedTag => {
        const tags = this.state.tags.filter(tag => tag !== removedTag);
        console.log(tags);
        this.setState({ tags });
    };

    showInput = () => {
        this.setState({ inputVisible: true }, () => this.input.focus());
    };
    
    handleInputChange = e => {
        this.setState({ inputValue: e.target.value });
    };


    handleInputConfirm = () => {
        const { inputValue } = this.state;
        let { tags } = this.state;
        if (inputValue && tags.indexOf(inputValue) === -1) {
          tags = [...tags, inputValue];
        }
        this.setState({
          tags,
          inputVisible: false,
          inputValue: '',
        });
    };

    saveInputRef = input => (this.input = input);


    handleChange = e => {
        if(e.target.files){
            this.setState({image:e.target.files[0]})

        }else{
            this.setState({[e.target.name]:e.target.value})

        }
    };
    
    back(){
        this.props.history.push('/home')
    }
    
    forMap = tag => {
        const tagElem = (
          <Tag
            closable
            onClose={e => {
              e.preventDefault();
              this.handleClose(tag);
            }}
          >
            {tag}
          </Tag>
        );
        return (
          <span key={tag} style={{ display: 'inline-block' }}>
            {tagElem}
          </span>
        );
      };
  

    handelSubmit=()=>{
        
        
        const {image, name, price, tags} = this.state
        const payload = {
            'name' : name,
            'price' : price,
            'tag' : tags
        }

        axios({
            method:"POST",
            url: 'http://localhost:8000/valuations/',
            data : payload,
            headers : {
                'Content-type' : 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        }).then(res=>{
            console.log(res)
            const formData =  new FormData()
            formData.append('image',image)
            formData.append('id', res.data)
            axios({
                method : "POST",
                url : 'http://localhost:8000/image/',
                data : formData,
                headers : {
                    'Content-type' : 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('access_token')}`
                }
            }).then(res=>{
                console.log(res)
                
                // Modal.success({
                // title:'평가생성에 성공했습니다.',
                // })
                // this.setState({name:''})
                // this.setState({price:null})
                // this.setState({image:null})
                // this.setState({tags:[]})
            })
            
    })
        .catch(err=>{
            Modal.error({
                title:'평가생성에 실패했습니다.',
                content:'다시 시도해주세요'
            })
        })
        
    }
    render(){
        const { tags, inputVisible, inputValue } = this.state;
        const tagChild = tags.map(this.forMap);

        return(
            <div>
                <PageHeader onBack={()=>this.back()} title='평가등록' />
                <Layout className='container' >
                    <div style={{width:'50%', textAlign:"center"}}>
                        <input type='file' onChange={e=>this.handleChange(e)} />
                        
                        <Input 
                        placeholder='평가 이름을 입력해주세요' 
                        name='name' 
                        onChange={e=>this.handleChange(e)}
                        value={this.state.name}
                        style={{textAlign:'center', marginTop:10}}
                        >

                        </Input>
                        <Input 
                        placeholder='평가의 예상금액을 입력해주세요(모르실 경우 0을 입력해주세요)' 
                        name='price'
                        onChange={e=>this.handleChange(e)}
                        value={this.state.price}
                        style={{textAlign:'center', marginTop:10}}

                        ></Input>
                        {tagChild}
                        {inputVisible && (
                        <Input
                            ref={this.saveInputRef}
                            type="text"
                            size="small"
                            style={{ width: 78 }}
                            value={inputValue}
                            onChange={this.handleInputChange}
                            onBlur={this.handleInputConfirm}
                            onPressEnter={this.handleInputConfirm}
                        />
                        )}
                        {!inputVisible && (
                        <Tag onClick={this.showInput} className="site-tag-plus">
                            <PlusOutlined /> New Tag
                        </Tag>
                        )}
                        <Button style={{marginTop:10}} onClick={this.handelSubmit}>평가 제출하기</Button>
                    </div>
                    
                </Layout>
                
            </div>
        )
    }
}