import React, {useState, useEffect} from 'react'
import {Input,Button, PageHeader, Layout, Modal, Result, Icon, notification} from 'antd'
import axios from 'axios'
import {useHistory} from 'react-router-dom'

const {Content} = Layout
export default function Valuation(){

    useEffect(()=>{
        getValuation()
    },[])
    const [image, setImage] = useState()
    const [name, setName] = useState()
    const [price, setPrice] = useState()
    const [id, setId] = useState()
    const [valuate, setValuate] = useState()
    const [isEmpty, setIsEmpty] = useState(false)
    function getValuation(){
        axios({
            method:'GET',
            url :'http://localhost:8000/valuations/',
            headers :{
                'Content-type' : 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        }).then(response=>{
            if(response.data==0){
                setIsEmpty(true)
            }else{
                setImage('http://localhost:8000'+response.data.image)
                setName(response.data.result.name)
                setPrice(response.data.result.price)
                setId(response.data.result.id)
            }
            
        }).catch(function(err){
            if(err.response.status== 401){
                history.push('/')
            }
        })
    }
    
    function setVisible(){
        if(isVisible){
            setIsVisible(false)
            getValuation()
                setValuate(null)
        }else{
            setIsVisible(true)
            
        }
    }

    function putValuation(){
        if(id){
            axios({
                method:'POST',
                url :`http://localhost:8000/valuate/${id}/`,
                headers :{
                    'Content-type' : 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('access_token')}`
                },
                data : {
                    'price' : valuate
                }
            }).catch(function(err){
                console.log(err)
            }).then(response => {
                setVisible()
            })
        }else{
            notification.error({message:'평가할 항목이 없습니다.'})
        }
        
    }
    const history = useHistory()
    const [isVisible , setIsVisible] = useState(false)
    const useInput = initialValue => {
        const [value, setValue] = useState(initialValue)
        const onChange = event => {
            const {target : {value}} = event
            setValue(value)

        }
        return {value, onChange}
    }
    return (
        <div >
            <PageHeader onBack={()=>history.push('/home')} title='평가하기' />
            <Layout className='container'>
                <Content style={{padding: '0 50px', width:'100%',textAlign:'center', marginTop:10}}>
                    {isEmpty?<Result
                            icon={<Icon type="smile" theme="twoTone" />}
                            title="모든 평가항목에 대한 평가를 완료했습니다!"
                            extra={<Button type="primary" onClick={e=>history.push('/upload')}>평가등록하러 가기</Button>}
                        />:<img src={image} style={{width:'50%', height:'50%'}}/>}
                    <p> 이름 </p> <p>{name}</p>
                    <p>예상 가격  </p> <Input placeholder='평가금액을 입력해주세요' value={valuate} style={{width:'50%', textAlign:"center"}}  onChange={e=>setValuate(e.target.value)} />
                    <p>기입된 가격  </p><p>기입이 되어진 금액은 평가 제출후 확인 가능합니다.</p>
                    <Button onClick={putValuation}>평가 제출</Button>
                </Content>
                <Modal visible={isVisible} onCancel={setVisible} title='기입된 가격' onOk={setVisible}><p>{price}</p></Modal>
            </Layout>
        </div>
        
    )
}