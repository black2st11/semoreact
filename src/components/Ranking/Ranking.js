import React from 'react'
import axios from 'axios'
import {List, Table, Layout, PageHeader, Typography} from 'antd'
import {useHistory} from 'react-router-dom'

const {Content} = Layout
const {title} = Typography
export default class Ranking extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            result : {},
            isLoading : true
        }
    }
    componentDidMount(){
        this.getValuationList()
    }

    getValuationList=()=>{
        axios({
            method:'GET',
            url :'http://localhost:8000/rank/',
            headers :{
                'Content-type' : 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        }).then(response=>{
            this.setState({result:response.data})
            this.setState({isLoading:false})
            console.log(this.state.result)
        }).catch(function(err){
            if(err.response){
                console.log(err.response)
                const history = useHistory()
                history.push('/')
            }
        })
    }
    render(){
    const {isLoading, result}  = this.state
    const columns = [
        {
            title : '평가이름',
            dataIndex : 'name',
            key :'name',
        },
        {
            title : '원래가격',
            dataIndex : 'price',
            key : 'price',
            render: (text) => <p>{parseInt(text).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}원</p>
        },
        {
            title : '이미지',
            dataIndex :'image',
            key : 'image',
            render : (text)=><img style={{width:50, height:50}} src={'http://localhost:8000/media/'+text} />
        },
        {
            title : '예상평균가격',
            dataIndex : 'average_price',
            key : 'average_price',
            render : (text) => <p>{parseInt(text).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}원</p>
        }
    ]
    
        if(!this.state.isLoading){
            return(
            <div>
                <PageHeader onBack={()=>this.props.history.push('/home')} title='랭킹페이지' />
                <Layout className='container' >
                    <Typography.Title>평가 순위</Typography.Title>

                    <Table style={{width:'70%'}} columns={columns} dataSource={result.data} />
                </Layout>
            </div>)
        }else{
           return <div>로딩중</div>
        }
        
    }
}




// export default function Ranking(){
    
//     const [isLoading, setLoading] = useState(true);
//     let title_array = []
//     let price_array = []
//     const [result,setResult] = useState()
//     useEffect(()=>{
//         getValuationList()
//     },[])
//     const getValuationList=()=>{
//         axios({
//             method:'GET',
//             url :'http://localhost:8000/rank/',
//             headers :{
//                 'Content-type' : 'application/json',
//                 'Authorization': `Bearer ${localStorage.getItem('access_token')}`
//             }
//         }).then(response=>{
//             setResult(response.data)
//         })
//     }
//     if(result){
//         title_array = result.origin_data
//         price_array = result.average_data
//     }
//     if (result){
//     return(
//         <div>
//             {title_array.map((value, index)=>{
//                 return (<p key={index}>{value}-{price_array[index]}</p>)
//             })}
//         </div>
//     )}else{
//         return null
//     }
// }