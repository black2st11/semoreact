import React from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Route} from 'react-router-dom'
import Login from './components/LoginForm/Login'
import SignUp from './components/Signup/SignUp'
import Home from './components/Home/Home'
import Ranking from './components/Ranking/Ranking'
import Valuation from './components/Valuation/Valuation'
import MyPage from './components/MyPage/MyPage'
import Board from './components/Board/Board'
import BoardDetail from './components/BoardDetail/BoardDetail'
import 'antd/dist/antd.css'
import UpValuation from './components/UpValuation/UpValuation';
export default class App extends React.Component{
  render(){
    return (
      <Router>
        <Route exact path='/' component={Login}/>
        <Route path='/signup' component={SignUp} />
        <Route path='/home' component={Home} />
        <Route path='/rank' component={Ranking} />
        <Route path='/valuation' component={Valuation} />
        <Route path='/mypage' component={MyPage} />
        <Route path='/upload' component={UpValuation} />
        <Route path='/board/id' component={BoardDetail} />
      </Router>
    )
  }
}

